


public class MainActivity extends AppCompatActivity {

    @VisibleForTesting
    protected static final String KEY_IMAGE_DATA = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new Draw(this));
    }
}


class Draw extends SurfaceView implements SurfaceHolder.Callback {

    public Draw(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        DrawThread thread = new DrawThread(this.getContext(), surfaceHolder);
        thread.start();


    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        x = (int)event.getX();
        y = (int) event.getY();
        r = 0;
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }
    class DrawThread extends Thread{
        private SurfaceHolder surfaceHolder;
        private volatile boolean running = true;
        Paint paint = new Paint();

        public DrawThread(Context context, SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }
        public void requestStop(){
            running=false;
        }

        @Override
        public void run() {
            while (running){
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    canvas.drawColor(Color.BLUE);
                    paint.setColor(Color.YELLOW);
                    canvas.drawCircle(x, y, r, paint);
                    getHolder().unlockCanvasAndPost(canvas);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}





